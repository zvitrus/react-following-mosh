import React, { Component } from "react";
import Products from "./Products/Products";
import nevada from "./Images/nevada.jpeg";
import chair from "./Images/chair.jpeg";
import apple from "./Images/apple.jpeg";

var productDetails = [
  {
    imageUrl: nevada,
    title: "NEVADA",
    description: "this is Nevada state!",
    price: 5
  },
  {
    imageUrl: nevada,
    title: "Chair",
    description: "this is Nevada state!",
    price: 6
  },
  {
    imageUrl: nevada,
    title: "Apple",
    description: "A deliciuous red apple",
    price: 3
  }
];

class App extends Component {
  render() {
    return (
      <div>
        <Products productsInputByUserArray={productDetails} />
      </div>
    );
  }
}

export default App;
